<?php declare(strict_types=1);

namespace TAS\SerializeServiceBundle\Service;

use JMS\Serializer\Handler\DateHandler;
use JMS\Serializer\JsonDeserializationVisitor;

class DateTimeHandler extends DateHandler
{
    public function deserializeDateTimeFromJson(JsonDeserializationVisitor $visitor, $data, array $type)
    {
        if(!$data){
            return null;
        }

        return parent::deserializeDateTimeFromJson($visitor, $data, $type);
    }
}