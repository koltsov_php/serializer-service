<?php declare(strict_types=1);

namespace TAS\SerializeServiceBundle\Service;

use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\VisitorInterface;
use TAS\BaseServiceBundle\Enum\AbstractEnum;
use TAS\BaseServiceBundle\Objects\HasDescriptionInterface;

class EnumHandler implements SubscribingHandlerInterface
{

    /**
     * Return format:
     *
     *      array(
     *          array(
     *              'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
     *              'format' => 'json',
     *              'type' => 'DateTime',
     *              'method' => 'serializeDateTimeToJson',
     *          ),
     *      )
     *
     * The direction and method keys can be omitted.
     *
     * @return array
     */
    public static function getSubscribingMethods()
    {
        $methods = [];
        $deserializationTypes = ['Enum'];
        $serialisationTypes = ['Enum'];

        foreach (['json'] as $format) {

            foreach ($deserializationTypes as $type) {
                $methods[] = [
                    'type'      => $type,
                    'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                    'format'    => $format,
                    'method'    => 'deserializeEnum',
                ];
            }

            foreach ($serialisationTypes as $type) {
                $methods[] = [
                    'type'      => $type,
                    'format'    => $format,
                    'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                    'method'    => 'serializeEnum',
                ];
            }
        }

        return $methods;
    }

    public function serializeEnum(VisitorInterface $visitor, AbstractEnum $enum, array $type, Context $context): array
    {
        $result = [
            'id' => $visitor->visitInteger($enum->getId(), $type, $context),
            'value' => $visitor->visitString($enum->getValue(), $type, $context),
        ];

        if($enum instanceof HasDescriptionInterface){
            $result['description'] = $visitor->visitString($enum->getDescription(), $type, $context);
        }

        return $result;
    }

    public function deserializeEnum(VisitorInterface $visitor, $data, array $type, Context $context)
    {
        return new $type['params'][0]['name']($data['value']);
    }
}