<?php declare(strict_types=1);

namespace TAS\SerializeServiceBundle\Service;

use Doctrine\ORM\PersistentCollection;
use JMS\Serializer\Accessor\AccessorStrategyInterface;
use JMS\Serializer\Context;
use JMS\Serializer\JsonDeserializationVisitor;
use JMS\Serializer\Metadata\PropertyMetadata;
use JMS\Serializer\Naming\PropertyNamingStrategyInterface;

class DoctrineDeserializerVisitor extends JsonDeserializationVisitor
{
    /**
     * DoctrineSerializerVisitor constructor.
     *
     * @param PropertyNamingStrategyInterface $namingStrategy
     * @param AccessorStrategyInterface|null  $accessorStrategy
     */
    public function __construct(
        PropertyNamingStrategyInterface $namingStrategy,
        AccessorStrategyInterface $accessorStrategy = null
    ) {
        parent::__construct($namingStrategy, $accessorStrategy);
    }

    public function visitProperty(PropertyMetadata $metadata, $data, Context $context)
    {
        $name = $this->namingStrategy->translateName($metadata);
        if (!array_key_exists($name, $data)) {
            return;
        }

        $collection = $this->accessor->getValue($this->getCurrentObject(), $metadata);
        if ($collection instanceof PersistentCollection && !empty($data)) {
            $ids = array_column($data[$name], 'id');
            foreach ($collection as $row) {
                if (array_search($row->getId(), $ids) === false) {
                    $collection->removeElement($row);
                }
            }
        }

        parent::visitProperty($metadata, $data, $context);
    }

}