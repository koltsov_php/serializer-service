<?php

namespace TAS\SerializeServiceBundle\Service;

use JMS\Serializer\ArrayTransformerInterface;
use JMS\Serializer\Construction\DoctrineObjectConstructor;
use JMS\Serializer\Construction\ObjectConstructorInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Handler\ArrayCollectionHandler;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\Naming\CacheNamingStrategy;
use JMS\Serializer\Naming\PropertyNamingStrategyInterface;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;

class DoctrineCustomSerializer implements SerializerInterface, ArrayTransformerInterface
{
    /**
     * @var \JMS\Serializer\Serializer
     */
    private $serializer;

    public function __construct(
        ObjectConstructorInterface $objectConstructor,
        PropertyNamingStrategyInterface $namingStrategy
    ) {
        $propertyNamingStrategy = new CacheNamingStrategy(new SerializedNameAnnotationStrategy($namingStrategy));
        $serializerBuilder = SerializerBuilder::create();

        if($objectConstructor instanceof DoctrineObjectConstructor){
            $visitor = new DoctrineDeserializerVisitor($propertyNamingStrategy, null);
            $serializerBuilder->setDeserializationVisitor('json', $visitor);
        }

        $this->serializer = $serializerBuilder
            ->setPropertyNamingStrategy($propertyNamingStrategy)
            ->setObjectConstructor($objectConstructor)
            ->configureHandlers(
                function (HandlerRegistry $registry) {
                    $registry->registerSubscribingHandler(new DateTimeHandler());
                    $registry->registerSubscribingHandler(new ArrayCollectionHandler());
                    $registry->registerSubscribingHandler(new EnumHandler());
                }
            )
            ->build();
    }

    /**
     * @inheritdoc
     */
    public function toArray($data, SerializationContext $context = null)
    {
        return $this->serializer->toArray($data, $context);
    }

    /**
     * @inheritdoc
     */
    public function fromArray(array $data, $type, DeserializationContext $context = null)
    {
        return $this->serializer->fromArray($data, $type, $context);
    }

    /**
     * @inheritdoc
     */
    public function serialize($data, $format, SerializationContext $context = null)
    {
        return $this->serializer->serialize($data, $format, $context);
    }

    /**
     * @inheritdoc
     */
    public function deserialize($data, $type, $format, DeserializationContext $context = null)
    {
        return $this->serializer->deserialize($data, $type, $format, $context);
    }
}